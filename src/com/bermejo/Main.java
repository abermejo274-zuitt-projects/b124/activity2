package com.bermejo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        int year;
        System.out.println("Please enter a year. (e.g. 2021)");
        year = appScanner.nextInt();

        if ((year % 4 == 0) && (year % 400 == 0) || (year % 100 != 0))
            System.out.println("The year you entered is a leap year");
        else
            System.out.println("The year you entered is not a leap year");

    }
}